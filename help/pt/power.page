<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="power" xml:lang="pt">

  <info>
    <link type="guide" xref="index#equation"/>
    <revision pkgversion="3.16" date="2015-02-21" status="candidate"/>

    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
      <years>2015</years>
    </credit>
    <credit type="editor">
      <name>Sebastian Rasmussen</name>
      <email its:translate="no">sebras@gmail.com</email>
      <years>2015</years>
    </credit>

    <desc>Aprender a introduzir raízes e potências na calculadora.</desc>
  </info>

  <title>Potências e raízes</title>
    
  <p>As potências introduzem-se introduzindo um <link xref="superscript">número superíndice</link> após o valor.</p>

  <example>
    <p>5²</p>
  </example>

  <p>A inversa de um número pode-se introduzir utilizando o símbolo de inverso ⁻¹ (<keyseq><key>Ctrl</key><key>I</key></keyseq>).</p>

  <example>
    <p>3⁻¹</p>
  </example>

  <p>As potências também se podem calcular utilizando o símbolo ^. Isto permite que a potência seja uma equação.</p>

  <example>
    <p>5^(6−2)</p>
  </example>

  <p>Se seu teclado não tem uma tecla <key>^</key> pode utilizar a tecla <key>*</key> duas vezes.</p>

  <p>Podem-se calcular as raízes quadradas utilizando o símbolo √ (<keyseq><key>Ctrl</key><key>R</key></keyseq>).</p>

  <example>
    <p>√2</p>
  </example>

  <p>Podem-se calcular as raízes enésimas pondo um <link xref="superscript">subíndice</link> dantes do símbolo de raiz.</p>

  <example>
    <p>₃√2</p>
  </example>

</page>
